package com.inholland.alexruslan.amsterdambeerguide;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.inholland.alexruslan.amsterdambeerguide.main.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainEspressoTest {
    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void TestBeerClick(){
        onView(withId(R.id.imageView1))
                .perform(click());
        onView(withId(R.id.fab))
                .perform(click());
    }

}
