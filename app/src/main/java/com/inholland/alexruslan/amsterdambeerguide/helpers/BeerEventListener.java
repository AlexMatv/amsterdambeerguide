package com.inholland.alexruslan.amsterdambeerguide.helpers;

import com.inholland.alexruslan.amsterdambeerguide.model.Beer;

public interface BeerEventListener {
    public void showBeerDetails(Beer beer);
}
