package com.inholland.alexruslan.amsterdambeerguide.helpers;

/**
 * Created by alex on 17-3-18.
 */

import com.loopj.android.http.*;

public class BreweryDBRestClient {
    private static final String BASE_URL = "http://api.brewerydb.com/v2/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}