package com.inholland.alexruslan.amsterdambeerguide.main.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.inholland.alexruslan.amsterdambeerguide.R;
import com.inholland.alexruslan.amsterdambeerguide.main.fragments.BreweriesListFragmentAll.OnListFragmentInteractionListener;
import com.inholland.alexruslan.amsterdambeerguide.helpers.BreweryEventListener;
import com.inholland.alexruslan.amsterdambeerguide.main.BreweryDetailsActivity;
import com.inholland.alexruslan.amsterdambeerguide.model.Brewery;

import com.inholland.alexruslan.amsterdambeerguide.databinding.FragmentBrewerieslistBinding;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Brewery} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class BreweriesListRecyclerViewAdapter extends RecyclerView.Adapter<BreweriesListRecyclerViewAdapter.ViewHolder> implements BreweryEventListener {

    private final List<Brewery> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context context;

    public BreweriesListRecyclerViewAdapter(List<Brewery> items, OnListFragmentInteractionListener listener, Context ctx) {
        mValues = items;
        mListener = listener;
        context = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FragmentBrewerieslistBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.fragment_brewerieslist, parent, false);

        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Brewery brewery = mValues.get(position);

        holder.breweryItemBinding.setBrewery(brewery);
        holder.breweryItemBinding.setItemClickListener(this);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void showBreweryDetails(Brewery brewery) {
        Log.e("!!!!!!!!!@@@@@######", brewery.name);
        Intent myIntent = new Intent(context, BreweryDetailsActivity.class);
        myIntent.putExtra("url", brewery.brewUrl);
        myIntent.putExtra("name", brewery.name);
        myIntent.putExtra("city", brewery.city);
        context.startActivity(myIntent);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public FragmentBrewerieslistBinding breweryItemBinding;

        public ViewHolder(FragmentBrewerieslistBinding fragmentBrewerieslistBinding) {
            super(fragmentBrewerieslistBinding.getRoot());
            breweryItemBinding = fragmentBrewerieslistBinding;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + breweryItemBinding.city.getText() + "'";
        }
    }
}
