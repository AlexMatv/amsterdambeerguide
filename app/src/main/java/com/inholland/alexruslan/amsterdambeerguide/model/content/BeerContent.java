package com.inholland.alexruslan.amsterdambeerguide.model.content;

import com.inholland.alexruslan.amsterdambeerguide.model.Beer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.inholland.alexruslan.amsterdambeerguide.main.MainActivity.beers;
//import static com.inholland.alexruslan.amsterdambeerguide.main.MainActivity.beersShuffled;

public class BeerContent {

    public static final List<Beer> ITEMS = new ArrayList<Beer>();

    public static final Map<String, Beer> ITEM_MAP = new HashMap<String, Beer>();

    private static final int COUNT = beers.size();

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createItem(i));
        }

    }

    private static void addItem(Beer item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static Beer createItem(int position) {
        String[] beer = beers.get(position);
        return new Beer(String.valueOf(position), beer[0] + " " + beer[1], beer[2]);
    }


//    private static String makeDetails(int position) {
//        StringBuilder builder = new StringBuilder();
//        builder.append("Details about Item: ").append(position);
//        for (int i = 0; i < position; i++) {
//            builder.append("\nMore details information here.");
//        }
//        return builder.toString();
//    }

}
