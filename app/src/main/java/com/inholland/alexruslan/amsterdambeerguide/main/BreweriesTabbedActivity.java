package com.inholland.alexruslan.amsterdambeerguide.main;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.inholland.alexruslan.amsterdambeerguide.R;
import com.inholland.alexruslan.amsterdambeerguide.main.fragments.BreweriesListFragmentAll;
import com.inholland.alexruslan.amsterdambeerguide.main.fragments.BreweriesListFragmentFavorites;
import com.inholland.alexruslan.amsterdambeerguide.main.fragments.BreweriesListFragmentFeatured;
import com.inholland.alexruslan.amsterdambeerguide.model.Brewery;

public class BreweriesTabbedActivity extends AppCompatActivity implements BreweriesListFragmentAll.OnListFragmentInteractionListener, BreweriesListFragmentFeatured.OnListFragmentInteractionListener, BreweriesListFragmentFavorites.OnListFragmentInteractionListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    static int onCreateCheck = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreateCheck = 1;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breweries_tabbed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        if (onCreateCheck == 1) {
            super.onResume();
            onCreateCheck = 0;
        } else {
            super.onResume();
            Log.e("^^^^^^^^^^^^^^^^", "onresume");

            mSectionsPagerAdapter.refreshData();
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_breweries_tabbed, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListFragmentInteraction(Brewery item) {
        
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.e("&&&&&&&&&@@@", Integer.toString(position));
            switch(position)
            {
                case 0: return BreweriesListFragmentFavorites.newInstance(1);
                case 1: return BreweriesListFragmentFeatured.newInstance(1);
                case 2: return BreweriesListFragmentAll.newInstance(1);
                default: return BreweriesListFragmentFavorites.newInstance(1);
            }
        }
//
        @Override
        public int getItemPosition(Object object) {
            Log.e("nksldnal", "CALLED");
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        public void refreshData() {
            //ITEMS.add(new Brewery("1", "2", "3", "4"));
            notifyDataSetChanged();
        }

    }

}
