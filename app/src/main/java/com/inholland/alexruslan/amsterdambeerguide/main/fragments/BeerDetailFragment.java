package com.inholland.alexruslan.amsterdambeerguide.main.fragments;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.json.*;

import com.inholland.alexruslan.amsterdambeerguide.R;
import com.inholland.alexruslan.amsterdambeerguide.helpers.BreweryDBRestClient;
import com.inholland.alexruslan.amsterdambeerguide.main.BeerDetailActivity;
import com.inholland.alexruslan.amsterdambeerguide.main.BeerListActivity;
import com.inholland.alexruslan.amsterdambeerguide.model.Beer;
import com.loopj.android.http.*;

import com.inholland.alexruslan.amsterdambeerguide.model.content.BeerContent;

import cz.msebera.android.httpclient.Header;

/**
 * A fragment representing a single Beer detail screen.
 * This fragment is either contained in a {@link BeerListActivity}
 * in two-pane mode (on tablets) or a {@link BeerDetailActivity}
 * on handsets.
 */
public class BeerDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    private Beer mItem;
    private View rootView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BeerDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            mItem = BeerContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.content);
            }
        }

        BreweryDBRestClientUsage restClient = new BreweryDBRestClientUsage();
        try {
            restClient.getBeerDetails(mItem.details);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.beer_detail, container, false);

        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.beer_detail)).setText("Waiting for API response for beer code " + mItem.details);
        }
        return rootView;
    }

    class BreweryDBRestClientUsage {
        public void getBeerDetails(String beerCode) throws JSONException {
            BreweryDBRestClient.get("beer/" + beerCode + "?key=964838e28cb2e9c2a18362ea1386aa2b&format=json", null, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    JSONObject data = null;
                    JSONObject style = null;
                    try {
                        data = (JSONObject) response.get("data");
                        if (!(data.isNull("style"))){
                            style = (JSONObject) data.get("style");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        String beerABV = "ABV is not available for this beer";
                        if (!(data.isNull("abv"))) {
                            beerABV = data.getString("abv");
                        }
                        String beerStyle = "Beer style is not available for this beer";
                        if (!(data.isNull("style")) && !(style.isNull("name"))) {
                                beerStyle = style.getString("name");
                        }
                        String beerDescription  = "Description is not available for this beer";
                        if (!(data.isNull("description"))) {
                            beerDescription = data.optString("description");
                        }
                        ((TextView) rootView.findViewById(R.id.beer_detail)).setText(
                                "Style: " + beerStyle + "\n\n" +
                                "ABV: " + beerABV + "%" +"\n\n" +
                                "Description: " + beerDescription
                                );

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

    //            @Override
    //            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
    //                // Pull out the first event on the public timeline
    //                JSONObject firstEvent = null;
    //                try {
    //                    firstEvent = (JSONObject) timeline.get(0);
    //                } catch (JSONException e) {
    //                    e.printStackTrace();
    //                }
    //                String text = null;
    //                try {
    //                    text = firstEvent.getString("text");
    //                } catch (JSONException e) {
    //                    e.printStackTrace();
    //                }
    //
    //                Log.e("response received!!!!!", text);
    //            }
            });
        }
    }
}