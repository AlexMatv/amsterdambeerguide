package com.inholland.alexruslan.amsterdambeerguide.main.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inholland.alexruslan.amsterdambeerguide.R;
import com.inholland.alexruslan.amsterdambeerguide.helpers.BeerEventListener;
import com.inholland.alexruslan.amsterdambeerguide.main.BeerDetailActivity;
import com.inholland.alexruslan.amsterdambeerguide.main.fragments.BeerDetailFragment;
import com.inholland.alexruslan.amsterdambeerguide.main.BeerListActivity;
import com.inholland.alexruslan.amsterdambeerguide.model.Beer;

import com.inholland.alexruslan.amsterdambeerguide.databinding.BeerListContentBinding;

import java.util.ArrayList;
import java.util.List;


public class BeerListRecyclerViewAdapter
        extends RecyclerView.Adapter<BeerListRecyclerViewAdapter.ViewHolder> implements BeerEventListener {

    private final BeerListActivity mParentActivity;
    private final List<Beer> mValues;
    private final List<Beer> itemsCopy;
    private final boolean mTwoPane;
    private Context context;

    public BeerListRecyclerViewAdapter(BeerListActivity parent, List<Beer> items, boolean twoPane, Context ctx) {
        mValues = items;
        itemsCopy = new ArrayList<>();
        itemsCopy.addAll(items);
        mParentActivity = parent;
        mTwoPane = twoPane;
        context = ctx;
    }

    public void filter(String text) {
        mValues.clear();
        if(text.isEmpty()){
            mValues.addAll(itemsCopy);
        } else{
            text = text.toLowerCase();
            for(Beer item: itemsCopy){
                if(item.content.toLowerCase().contains(text) || item.details.toLowerCase().contains(text)){
                    mValues.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        BeerListContentBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.beer_list_content, parent, false);

        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Beer beer = mValues.get(position);

        holder.beerItemBinding.setBeer(beer);
        holder.beerItemBinding.setItemClickListener(this);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public void showBeerDetails(Beer beer) {
        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putString(BeerDetailFragment.ARG_ITEM_ID, beer.id);
            BeerDetailFragment fragment = new BeerDetailFragment();
            fragment.setArguments(arguments);
            mParentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.beer_detail_container, fragment)
                    .commit();
        } else {
            Intent intent = new Intent(context, BeerDetailActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(BeerDetailFragment.ARG_ITEM_ID, beer.id);

            context.startActivity(intent);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public BeerListContentBinding beerItemBinding;

        public ViewHolder(BeerListContentBinding beerListContentBinding) {
            super(beerListContentBinding.getRoot());
            beerItemBinding = beerListContentBinding;
        }
    }
}
