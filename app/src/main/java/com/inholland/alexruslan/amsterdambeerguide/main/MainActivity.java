package com.inholland.alexruslan.amsterdambeerguide.main;

import com.inholland.alexruslan.amsterdambeerguide.R;
import com.inholland.alexruslan.amsterdambeerguide.databinding.ActivityMainBinding;

import android.databinding.DataBindingUtil;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.inholland.alexruslan.amsterdambeerguide.helpers.DataBaseHelper;
import com.inholland.alexruslan.amsterdambeerguide.model.Brewery;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static ArrayList<Brewery> breweries = new ArrayList<>();
    public static ArrayList<Brewery> featured = new ArrayList<>();
    public static ArrayList<Brewery> favorites = new ArrayList<>();
    public static Map<Integer, String[]> beers;
    public static ArrayList<String[]> coordinates = new ArrayList<>();
    //public static Map<Integer, String[]> beersShuffled = new TreeMap<>();
    static final ArrayList<ImageView> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding bindings = DataBindingUtil.setContentView(this, R.layout.activity_main);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);

        //setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setSeeAllButtons();

        //DataBaseHelper myDbHelper = new DataBaseHelper(this.getApplicationContext());
        DataBaseHelper myDbHelper = new DataBaseHelper(this);
        setUpDataBase(myDbHelper);

        SQLiteDatabase db = myDbHelper.myDataBase;
        //ArrayList<Brewery> testBrew = readBreweriesFromDB(db);

        breweries = readBreweriesFromDB(db);
        featured = populateBrewArray("featured");
        favorites = populateBrewArray("favorites");
        beers = populateBeerMap();
        coordinates = populateCoordinatesArray();

        //shuffleBeers();
        setImages();
        setLinksToImages();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main) {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
        } else if (id == R.id.nav_breweries) {

            Intent intent = new Intent(this, BreweriesTabbedActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_beers) {

            Intent intent = new Intent(this, BeerListActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_map) {

            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_photo) {

            Intent intent = new Intent(this, GalleryActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_rate) {

        } else if (id == R.id.nav_about) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void setImages(){
        String imageString = "imageView";
        for (int i = 1; i<7; i++) {
            String imageStringIndex = imageString.concat(Integer.toString(i));
            int resID = getResources().getIdentifier(imageStringIndex , "id", getPackageName());
            images.add((ImageView) findViewById(resID));
        }
    }

    void setLinksToImages(){
        for (final ImageView img: images) {
            final String brewUlrMain;
            switch(images.indexOf(img)) {
                case 0:
                    brewUlrMain = breweries.get(44).brewUrl;
                    break;
                case 1:
                    brewUlrMain = breweries.get(7).brewUrl;
                    break;
                case 2:
                    brewUlrMain = breweries.get(4).brewUrl;
                    break;
                case 3:
                    brewUlrMain = breweries.get(52).brewUrl;
                    break;
                case 4:
                    brewUlrMain = breweries.get(43).brewUrl;
                    break;
                case 5:
                    brewUlrMain = breweries.get(20).brewUrl;
                    break;
                default:
                    brewUlrMain = breweries.get(44).brewUrl;
                    break;
            }
            img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent myIntent = new Intent(v.getContext(), BreweryDetailsActivity.class);
                    myIntent.putExtra("url", brewUlrMain);
                    v.getContext().startActivity(myIntent);
                }
            });
        }
    }

    void setUpDataBase(DataBaseHelper dbHelper) {
        try {
            dbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        try {
            dbHelper.openDataBase();

        }catch(SQLException sqle){
            throw sqle;

        }
    }

    ArrayList<Brewery> readBreweriesFromDB(SQLiteDatabase db) {
        Cursor cursor = db.query("allbr", null,
                null, null, null, null, null);

        //Log.e("!!!!!!", Integer.toString(cursor.getCount()));
        ArrayList<Brewery> brewArray = new ArrayList<>();
        int breweriesCount = 1;
        if (cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String city = cursor.getString(cursor.getColumnIndex("city"));
                String url = cursor.getString(cursor.getColumnIndex("url"));
                //Log.e("!!!!!!!!!!!!!", name+city+url);
                Brewery brewery = new Brewery(Integer.toString(breweriesCount), name, city, url);
                brewArray.add(brewery);
                breweriesCount++;
                cursor.moveToNext();
            }
        }
        cursor.close();
        return brewArray;
    }

    TreeMap<Integer, String[]> populateBeerMap() {

        TreeMap<Integer, String[]> beersMap = new TreeMap<>();
        String[] details;
        InputStream inputStream = getResources().openRawResource(R.raw.beers);
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            int lines = 0;
            br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {

                details = line.split(cvsSplitBy);
                beersMap.put(lines+1, details);
                lines++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return beersMap;
    }

    ArrayList<Brewery> populateBrewArray(String resCode) {
        String[] details;
        InputStream inputStream;

        switch (resCode) {
            case "all":
                inputStream = getResources().openRawResource(R.raw.breweries_with_url);
                break;
            case "featured":
                inputStream = getResources().openRawResource(R.raw.breweries_with_url_featured);
                break;
            case "favorites":
                inputStream = getResources().openRawResource(R.raw.breweries_with_url_favorites);
                break;
            default:
                inputStream = getResources().openRawResource(R.raw.breweries_with_url);
                break;
        }

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        ArrayList<Brewery> brewArray = new ArrayList<>();

        try {
            int lines = 0;
            br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {

                details = line.split(cvsSplitBy);
                Brewery brewery = new Brewery(Integer.toString(lines), details[0], details[1], details[2]);
                brewArray.add(brewery);
                lines++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return brewArray;
    }

    ArrayList<String[]> populateCoordinatesArray() {
        String[] details;
        InputStream inputStream = getResources().openRawResource(R.raw.coordinates);

        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<String[]> coordArray = new ArrayList<>();

        try {
            int lines = 0;
            br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {

                details = line.split(cvsSplitBy);
                coordArray.add(details);
                lines++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return coordArray;
    }

    void setSeeAllButtons() {
        TextView seeAllBrew = (TextView) findViewById(R.id.seeAllBreweries);
        TextView seeAllBeer = (TextView) findViewById(R.id.seeAllBeers);

        seeAllBrew.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), BreweriesTabbedActivity.class);
                startActivity(intent);
            }
        });

        seeAllBeer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), BeerListActivity.class);
                startActivity(intent);
            }
        });
    }

//    void shuffleBeers() {
//        List keys = new ArrayList(beers.keySet());
//        Collections.shuffle(keys);
//        for (Object o : keys) {
//            String[] b = beers.get(o);
//            beersShuffled.put((Integer) o, b);
//        }
//    }
}
