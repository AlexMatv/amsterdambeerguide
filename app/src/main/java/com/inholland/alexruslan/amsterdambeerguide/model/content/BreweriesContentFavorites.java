package com.inholland.alexruslan.amsterdambeerguide.model.content;

import com.inholland.alexruslan.amsterdambeerguide.model.Brewery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.inholland.alexruslan.amsterdambeerguide.main.MainActivity.favorites;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class BreweriesContentFavorites {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Brewery> ITEMS = new ArrayList<Brewery>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, Brewery> ITEM_MAP = new HashMap<String, Brewery>();

    private static int COUNT = favorites.size();

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createBrewery(i));
        }
    }

    private static void addItem(Brewery item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.brewId, item);
    }

    private static Brewery createBrewery(int position) {
        //return new Brewery(breweries.get(position - 1).brewId, "Item " + position + "blahblah", makeDetails(position), "");
        return favorites.get(position - 1);
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
}
