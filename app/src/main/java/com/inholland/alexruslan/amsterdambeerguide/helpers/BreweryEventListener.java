package com.inholland.alexruslan.amsterdambeerguide.helpers;

import com.inholland.alexruslan.amsterdambeerguide.model.Brewery;

public interface BreweryEventListener {
    public void showBreweryDetails(Brewery brewery);
}
