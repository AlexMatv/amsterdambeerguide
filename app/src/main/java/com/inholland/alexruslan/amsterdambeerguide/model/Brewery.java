package com.inholland.alexruslan.amsterdambeerguide.model;

/**
 * Created by almatv on 07.03.2018.
 */

public class Brewery {

        public final String brewId;
        public final String name;
        public final String city;
        public final String brewUrl;

        public Brewery(String id, String name, String city, String url) {
            this.brewId = id;
            this.name = name;
            this.city = city;
            this.brewUrl = url;
        }

        @Override
        public String toString() {
            return name;
        }
}
