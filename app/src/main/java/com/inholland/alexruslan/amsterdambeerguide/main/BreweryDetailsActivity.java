package com.inholland.alexruslan.amsterdambeerguide.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.inholland.alexruslan.amsterdambeerguide.R;
import com.inholland.alexruslan.amsterdambeerguide.model.content.BreweriesContentFavorites;
import com.inholland.alexruslan.amsterdambeerguide.model.Brewery;

import static com.inholland.alexruslan.amsterdambeerguide.main.MainActivity.favorites;

public class BreweryDetailsActivity extends AppCompatActivity {

    public static String encoding = "utf-8";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brewery_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        final String brewUrl = intent.getStringExtra("url");
        final String brewName = intent.getStringExtra("name");
        final String brewCity = intent.getStringExtra("city");
        //Log.d("intentTest", value);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(view.getContext());
                builder1.setMessage("Add to favourite breweries?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                BreweriesContentFavorites.ITEMS.add(new Brewery(Integer.toString(favorites.size()), brewName, brewCity, brewUrl));
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        final WebView webview = (WebView) findViewById(R.id.brewery_webpage);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onLoadResource(WebView view, String url)
            {
                webview.loadUrl("javascript:(function() { " +
                        "document.getElementById(\"headerCntr\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.getElementById(\"menu\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.getElementById(\"Bier_Map\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.querySelector(\".vorigevolgende\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.querySelector(\".boxAddthis.center.margintop15\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.querySelector(\".box.center.margintop15\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.querySelector(\".boxreactie\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.getElementsByTagName(\"footer\")[0].style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.querySelector(\".bancenter\").style.display=\"none\"; " +
                        "})()");
                webview.loadUrl("javascript:(function() { " +
                        "document.getElementById(\"trigger_maps\").style.display=\"none\"; " +
                        "})()");
            }
        });
        //webview.loadUrl("https://www.biernet.nl/bier/brouwerijen/nederland/noord-holland/amsterdam/kleiburg");
        webview.loadUrl(brewUrl);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
