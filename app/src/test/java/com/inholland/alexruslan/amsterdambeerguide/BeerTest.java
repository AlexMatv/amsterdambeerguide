package com.inholland.alexruslan.amsterdambeerguide;

import com.inholland.alexruslan.amsterdambeerguide.model.Beer;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.mock;

public class BeerTest {
    private Beer mBeer;
    private String mSubject;

    @Before
    public void setUp() throws Exception {
        mBeer = new Beer("1", "Warsteiner", "4.5 alc");
        System.out.println(mBeer.content);
        mSubject = mBeer.toString();
        System.out.println(mSubject);
    }

    @Test
    public void testToString() {
        assertEquals(mBeer.content, mSubject);
    }
}
