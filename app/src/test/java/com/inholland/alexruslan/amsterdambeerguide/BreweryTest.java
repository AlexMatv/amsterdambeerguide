package com.inholland.alexruslan.amsterdambeerguide;

import com.inholland.alexruslan.amsterdambeerguide.model.Beer;
import com.inholland.alexruslan.amsterdambeerguide.model.Brewery;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.mock;

public class BreweryTest {
    private Brewery mBrewery;
    private String mSubject;

    @Before
    public void setUp() throws Exception {
        mBrewery = new Brewery("1", "Warsteiner", "na", "www.site.com");
        mSubject = mBrewery.toString();
        System.out.println(mSubject);
    }

    @Test
    public void testToString() {
        assertEquals(mBrewery.name, mSubject);
    }
}
